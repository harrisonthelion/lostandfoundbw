# Global Game Jam 2021 - Black and White (name TBD)

## 2D Game Made with Unity

### Contributors
* Alex Lee
* Harrison Leon (you)
* Ben Chrisman
* Connor Cook
* Logan Stahley
* Stephanie Chan
* Tracy Kawabata

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact