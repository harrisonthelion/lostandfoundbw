﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreboardUI : MonoBehaviour
{
    [SerializeField]
    private Text magentaScoreText;
    [SerializeField]
    private Text blackScoreText;
    [SerializeField]
    private Text messageText;
    [SerializeField]
    private Text countdownText;
    [SerializeField]
    private GameObject countdownBackground;


    private void Start()
    {
        messageText.text = "";
        countdownText.text = "";
        countdownBackground.SetActive(false);

        magentaScoreText.text = "PRESS A TO JOIN";
        blackScoreText.text = "PRESS A TO JOIN";
    }

    public void SetMagentaPlayerJoined(bool joined)
    {
        if (joined)
        {
            magentaScoreText.text = "PRESS A TO READY UP";
        }
    }

    public void SetBlackPlayerJoined(bool joined)
    {
        if (joined)
        {
            blackScoreText.text = "PRESS A TO READY UP";
        }
    }


    public void SetMagentaScore(int score)
    {
        magentaScoreText.text = "EARL MOONBEAM - " + score;
    }

    public void SetBlackScore(int score)
    {
        blackScoreText.text = score + " - STELLA ANDROMEDA";
    }

    public void SetNumReadyPlayers(int ready, int max)
    {
        messageText.color = Color.white;
        messageText.text = "Num Ready: " + ready + "/" + max;
    }

    public void StartMatch()
    {
        messageText.text = "FIGHT!";
        blackScoreText.text = "0 - STELLA ANDROMEDA";
        magentaScoreText.text = "EARL MOONBEAM - 0";
    }

    private IEnumerator StartMatchRoutine()
    {
        yield return new WaitForSeconds(2.0f);
        ClearScoreboardMessage();
    }

    public void SetScoreboardMessage(string msg, Color msgColor)
    {
        messageText.text = msg;
        messageText.color = msgColor;
    }

    public void ClearScoreboardMessage()
    {
        messageText.text = "";
        countdownText.text = "";
        countdownBackground.SetActive(false);
    }

    public void SetCountdownText(int sec)
    {
        countdownText.text = sec.ToString();
        countdownBackground.SetActive(true);
    }
}
