﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PressToStartScript : MonoBehaviour
{
    public string gameplaySceneName;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnSubmit()
    {
        if (gameplaySceneName.Length > 0)
        {
            if (FindObjectOfType<IntroVideoTimerScript>() != null)
            {
                if (FindObjectOfType<IntroVideoTimerScript>().GetAllowInput())
                {
                    Debug.Log("GO TO GAMEPLAY SCENE");
                    // Application.LoadLevel(gameplaySceneName);
                    SceneManager.LoadScene(gameplaySceneName);
                }
            }
        }

    }
}
