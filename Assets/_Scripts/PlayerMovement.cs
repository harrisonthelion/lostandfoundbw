﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour
{
    public bool canMove = false;
    // move input
    Vector2 moveInput;
    Vector2 calculatedMovement;

    public float movementSpeed = 1f;
    //public AudioClip footsteps;
    public AudioSource footstepSource;

    private bool isMoving;

    [HideInInspector]
    public Vector2 inputMovement;
    [HideInInspector]
    public Rigidbody2D rigidbody;
    [HideInInspector]
    public Animator anim;
    [HideInInspector]
    public float hf = 0.0f;
    [HideInInspector]
    public float vf = 0.0f;

    private Vector2 playerBoundsX = new Vector2(-100.0f, 100.0f);
    private Vector2 playerBoundsY = new Vector2(-100.0f, 100.0f);

    private void Awake()
    {
        isMoving = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        rigidbody = this.GetComponent<Rigidbody2D>();
        rigidbody.freezeRotation = true;
        anim = this.GetComponent<Animator>();
    }

    public void SetBounds(Vector2 boundsX, Vector2 boundsY)
    {
        playerBoundsX = boundsX;
        playerBoundsY = boundsY;
    }

    // Update is called once per frame
    void Update()
    {
        if (canMove)
        {
            Move();
            ClampPosition();
        }
    }

    private void Move()
    {
        calculatedMovement = inputMovement.normalized;

        hf = calculatedMovement.x > 0.01f ? calculatedMovement.x : calculatedMovement.x < -0.01f ? 1 : 0;
        vf = calculatedMovement.y > 0.01f ? calculatedMovement.y : calculatedMovement.y < -0.01f ? 1 : 0;

        if (calculatedMovement.x < -0.01f)
        {
            this.gameObject.transform.localScale = new Vector3(-1, 1, 1);

        }
        else
        {
            this.gameObject.transform.localScale = new Vector3(1, 1, 1);
        }

        anim.SetFloat("Horizontal", hf);
        anim.SetFloat("Vertical", calculatedMovement.y);
        anim.SetFloat("Speed", vf);
    }

    private void ClampPosition()
    {
        float clampedX = Mathf.Clamp(this.gameObject.transform.position.x, playerBoundsX.x, playerBoundsX.y);
        float clampedY = Mathf.Clamp(this.gameObject.transform.position.y, playerBoundsY.x, playerBoundsY.y);
        this.gameObject.transform.position = new Vector3(clampedX, clampedY, 0.0f);
    }

    private void FixedUpdate()
    {
        //rigidbody.MovePosition(rigidbody.position + inputMovement * movementSpeed);
        rigidbody.velocity = inputMovement * movementSpeed;

        if (rigidbody.velocity.magnitude != 0f && !isMoving)
        {
            footstepSource.Play();
            isMoving = true;
        }

        else if (rigidbody.velocity.magnitude == 0f && isMoving)
        {
            footstepSource.Stop();
            isMoving = false;
        }

    }

    private void OnMove(InputValue value)
    {
        if (canMove)
        {
            inputMovement = value.Get<Vector2>();
        }
    }
}

