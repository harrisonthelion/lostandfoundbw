﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileBehavior : MonoBehaviour
{

    public string tileType;

    public Sprite magentaSprite;
    public Sprite blackSprite;
    public Sprite neutralSprite;

    private SpriteRenderer rend;

    private void Awake()
    {
        rend = GetComponent<SpriteRenderer>();
    }


    public void FlipTile(string color)
    {
        Debug.Log("flip called " + color);
        if (color == "Magenta")
        {
            Debug.Log("Magenta Entered");
            rend.sprite = magentaSprite;
            this.gameObject.tag = "Magenta";
        }
        else if (color == "Black")
        {
            Debug.Log("Black Entered");
            rend.sprite = blackSprite;
            this.gameObject.tag = "Black";
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        // Debug.Log("tile registered hit");
    }

}
