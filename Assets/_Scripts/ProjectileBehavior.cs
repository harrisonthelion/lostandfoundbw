﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileBehavior : MonoBehaviour
{
    //private Rigidbody2D rigidbody;

    public int damage;
    public float movementSpeed;

    // used to let us know who fired this projectile (to prevent self damage)
    [HideInInspector]
    public GameObject instigator;

    private void Start()
    {
        //rigidbody = GetComponent<Rigidbody2D>();
    }


    private void Update()
    {
        //rigidbody.velocity = transform.forward * movementSpeed;
        //rigidbody.AddForce(Vector3.forward * movementSpeed);
        transform.position += transform.up * Time.deltaTime * movementSpeed;
    }

    private void OnTriggerEnter2D(Collider2D collider2d)
    {
        // make sure not to blow up yourself
        if (collider2d.tag == "Player" && instigator != collider2d.gameObject)
        {
            Debug.Log("Hit");
            collider2d.GetComponent<Player>().Damage(damage);//deal damage
            Destroy(this.gameObject);   //destroy arrow
        }

        else if (collider2d.tag == "Wall")
        {
            Destroy(this.gameObject);   //destroy arrows
        }
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        // make sure not to blow up yourself
        if (collision.gameObject.tag == "Player" && instigator != collision.gameObject)
        {
            Debug.Log("Hit");
            collision.gameObject.GetComponent<Player>().Damage(damage);//deal damage
            Destroy(this.gameObject);   //destroy arrow
        }

        else if (collision.gameObject.tag == "Wall")
        {
            Destroy(this.gameObject);   //destroy arrows
        }
    }
}
