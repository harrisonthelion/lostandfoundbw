﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{

    public AudioSource intro;
    public AudioSource battleMusic;
    public AudioSource idleMusic;
    public AudioSource death;

    public float musicDelay = 3f;

    private float timer;
    private bool musicStarted;
    private bool introTriggered;

    void Awake()
    {
        timer = 0f;
        musicStarted = false;
        introTriggered = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (timer < musicDelay && introTriggered)
        {
            timer += Time.deltaTime;
        }

        else if (timer >= musicDelay && !musicStarted)
        {
            battleMusic.Play();
            musicStarted = true;
        }
    }

    public void PlayDeathSound()
    {
        death.Play();
    }

    public void PlayIntro()
    {
        StartCoroutine(IntroRoutine());
        //intro.Play();
    }

    IEnumerator IntroRoutine()
    {
        intro.Play();

        yield return new WaitForSeconds(musicDelay);

        intro.Stop();
        battleMusic.Play();
    }

    public void PlayBattleMusic()
    {
        battleMusic.Play();
    }

    public void StopBattleMusic()
    {
        battleMusic.Stop();
        introTriggered = false;
        timer = 0f;
        musicStarted = false;
    }


    public void PlayIdleMusic()
    {
        idleMusic.Play();
    }

    public void StopIdleMusic()
    {
        idleMusic.Stop();
    }
}
