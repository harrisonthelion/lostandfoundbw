﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [Header("Game Rules")]
    public int maxNumPlayers = 2;
    public int pointsToWin = 5;

    [Header("Spawns and Boundaries")]
    public List<Vector3> spawnPointList;
    public Vector2 playerBoundsX;
    public Vector2 playerBoundsY;

    List<PlayerState> connectedPlayers;

    private GameState gameState;
    
    // Awake is called when the script instance is being loaded
    private void Awake()
    {
        connectedPlayers = new List<PlayerState>();

        // setup game state vars
        gameState = GetComponent<GameState>();
    }
    
    // Player ID Tracking ----
    public void AddPlayer(PlayerState player)
    {
        connectedPlayers.Add(player);
        gameState.AddPlayer(player);
    }

    public void RemovePlayer(PlayerState player)
    {
        connectedPlayers.Remove(player);
        gameState.RemovePlayer(player);
    }

    public int GetNumConnectedPlayers()
    {
        return connectedPlayers.Count;
    }

    public void RespawnAllPlayers()
    {
        List<Vector3> spawnPoints;
        if (GetRandomizedSpawnPoints(out spawnPoints))
        {
            int i = 0;
            foreach (PlayerState playerState in connectedPlayers)
            {
                playerState.RespawnPlayer(spawnPoints[i]);
                i++;
            }
        }
        else
        {
            Debug.LogError("Respawning all players failed");
        }

    }

    public void SetAllPlayersCanMove(bool canMove)
    {
        foreach (PlayerState playerState in connectedPlayers)
        {
            playerState.gameObject.GetComponent<PlayerMovement>().canMove = canMove;
        }
    }

    private bool GetRandomizedSpawnPoints(out List<Vector3> randomSpawns)
    {
        randomSpawns = new List<Vector3>();
        System.Random random = new System.Random();
        int numSpawns = GetNumConnectedPlayers();

        if (numSpawns <= spawnPointList.Count)
        {
            for (int i = 0; i < numSpawns; i++)
            {
                // randomSpawns.Add(spawnPointList[random.Next(0, spawnPointList.Count + 1)]);
                randomSpawns.Add(spawnPointList[i]);
            }
            
            return true;
        }

        Debug.LogError("GetRandomizedSpawnPoints: Not enough spawn points to choose from, deafulting to zero vector");
        for (int i = 0; i < numSpawns; i++)
        {
            randomSpawns.Add(Vector3.zero);
        }

        return false;
    }
    // ----
}
