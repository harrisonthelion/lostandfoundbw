﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerDirection : MonoBehaviour
{
    Vector2 aim;

    public GameObject compass;

    private void Awake()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Aim();
    }

    private void Aim()
    {
        if (aim.magnitude > 0 || aim.magnitude < 0)
        {
            float angle = Mathf.Atan2(aim.y, aim.x) * Mathf.Rad2Deg;
            //Debug.Log(angle);
            compass.transform.rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);
        }
    }

    private void OnAimAnalog(InputValue value)
    {
        aim = value.Get<Vector2>();
    }
}
