﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerColor
{
    Black,
    Magenta
}

public class PlayerState : MonoBehaviour
{
    [Header("Sprite Options")]
    public Sprite blackSprite;
    public Sprite magentaSprite;

    [Header("Animator Options")]
    [SerializeField]
    private RuntimeAnimatorController blackAnimController;
    [SerializeField]
    private RuntimeAnimatorController magentaAnimController;

    [Header("Reference Varaibles")]
    [SerializeField]
    private Animator animatorReference;
    [SerializeField]
    private SpriteRenderer spriteRendererReference;
    
    private GameManager gameManager;
    private GameState gameState;

    private Player player;
    
    private int playerID;
    private int score;

    public PlayerColor assignedColor = PlayerColor.Magenta;

    private void OnJoin()
    {
        GetGameState().AddReadyPlayer(this);
    }
    
    private void Awake()
    {
        player = GetComponent<Player>();
    }

    public void RespawnPlayer(Vector3 SpawnLocation)
    {
        // ensure valid 2D spawn
        SpawnLocation.z = 0.0f;
        this.gameObject.transform.position = SpawnLocation;
        player.Respawn();
    }

    public void SetScore(int score)
    {
        this.score = score;
    }

    private void Start()
    {
        SetupPlayer();
        GetGameManager().RespawnAllPlayers();
    }

    public void SetAliveStatus(bool alive)
    {
        if (!alive)
        {
            GetGameState().MarkPlayerDead(this);
        }
    }

    public void SetupPlayer()
    {
        GetGameManager().AddPlayer(this);
        score = 0;

        // setup player walking bounds
        player.SetMovementBounds(GetGameManager().playerBoundsX, GetGameManager().playerBoundsY);

        // setup color here
        assignedColor = GetGameState().GetNeededColor();

        // set team color in Player.cs
        switch (assignedColor)
        {
            case PlayerColor.Black:
                GetComponent<Player>().teamColor = "Black";
                spriteRendererReference.sprite = blackSprite;
                animatorReference.runtimeAnimatorController = blackAnimController;
                FindObjectOfType<ScoreboardUI>().SetBlackPlayerJoined(true);
                break;

            case PlayerColor.Magenta:
                GetComponent<Player>().teamColor = "Magenta";
                spriteRendererReference.sprite = magentaSprite;
                animatorReference.runtimeAnimatorController = magentaAnimController;
                FindObjectOfType<ScoreboardUI>().SetMagentaPlayerJoined(true);
                break;
                
            default:
                break;
        }
    }

    public void RemovePlayer()
    {
        GetGameManager().RemovePlayer(this);
    }

    // Set Data ----
    public void SetPlayerID(int id)
    {
        playerID = id;
    }

    // Get Data ----
    public int GetPlayerID()
    {
        return playerID;
    }

    public int GetScore()
    {
        return score;
    }

    // Helper functions
    private GameManager GetGameManager()
    {
        if (gameManager != null)
        {
            return gameManager;
        }
        else
        {
            gameManager = FindObjectOfType<GameManager>();
            if (gameManager != null)
            {
                return gameManager;
            }
            else
            {
                Debug.LogError("PlayerState: GameManager could not be found, place one in the scene.");
                return null;
            }
        }
    }

    private GameState GetGameState()
    {
        if (gameState != null)
        {
            return gameState;
        }
        else
        {
            gameState = FindObjectOfType<GameState>();
            if (gameState != null)
            {
                return gameState;
            }
            else
            {
                Debug.LogError("PlayerState: gameState could not be found, place one in the scene.");
                return null;
            }
        }
    }
}
