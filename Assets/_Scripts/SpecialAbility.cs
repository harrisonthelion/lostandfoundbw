﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialAbility : MonoBehaviour
{
    public int maxAmmo;
    public float reloadTime;
    public float fireRate;

    public GameObject ammoType;
    public GameObject bulletSpawn;
    public AudioClip fireSound;
    //public AudioClip emptySound;

    public int currentAmmo;

    private float timeSinceFire;

    private void Awake()
    {
        currentAmmo = maxAmmo;
    }

    void Update()
    {
        timeSinceFire += Time.deltaTime;
    }

    private void Fire()
    {
        //Debug.Log("Fire");
        if (currentAmmo > 0 && timeSinceFire > fireRate)
        {
            GameObject grenade = Instantiate(ammoType, bulletSpawn.transform.position, bulletSpawn.transform.rotation);
            grenade.GetComponent<GrenadeBehavior>().color = GetComponent<Player>().teamColor;
            currentAmmo--;
            timeSinceFire = 0f;
            GetComponent<AudioSource>().PlayOneShot(fireSound);
        }

        else
        {
            Debug.Log("No Ammo");
            //GetComponent<AudioSource>().PlayOneShot(emptySound);
        }
    }

    private void OnSpecial()
    {
        Fire();
    }

    public void InstantReload()
    {
        currentAmmo = maxAmmo;
    }


    public IEnumerator Reload()
    {
        float timeElapsed = 0f;

        while (timeElapsed < reloadTime)
        {
            timeElapsed += Time.deltaTime;

            if (timeElapsed >= reloadTime)
            {
                if (currentAmmo < maxAmmo)
                {
                    currentAmmo++;
                }

                timeElapsed = 0f;
            }

            yield return null;
        }

    }


}
