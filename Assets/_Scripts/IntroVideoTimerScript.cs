﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroVideoTimerScript : MonoBehaviour
{
    public float timeBeforeAllowInput = 45.0f;
    private bool allowInput = false;

    public GameObject pressToStartPanel;

    // Start is called before the first frame update
    void Start()
    {
        if (pressToStartPanel != null)
        {
            pressToStartPanel.SetActive(false);
        }

        StartCoroutine(ShowPressToStartPanel());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator ShowPressToStartPanel()
    {
        yield return new WaitForSeconds(timeBeforeAllowInput);

        pressToStartPanel.SetActive(true);
        allowInput = true;
    }

    public bool GetAllowInput()
    {
        return allowInput;
    }
}
