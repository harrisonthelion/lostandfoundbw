﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState : MonoBehaviour
{   
    [SerializeField]
    private ScoreboardUI scoreboard;

    [SerializeField]
    private GameObject earlVictoryPanel;
    [SerializeField]
    private GameObject stellaVictoryPanel;
    
    private int maxPlayers;
    int numReadyPlayers = 0;
    int blackScore = 0;
    int magentaScore = 0;

    private int numBlackPlayers = 0;
    private int numMagentaPlayers = 0;

    // runtime variables
    int numAliveMagenta = 0;
    int numAliveBlack = 0;

    Dictionary<PlayerState, bool> readyPlayerDictionary;

    public void AddReadyPlayer(PlayerState playerState)
    {
        readyPlayerDictionary[playerState] = true;
        numReadyPlayers++;
        CheckAllPlayersReady();
    }

    private bool CheckAllPlayersReady()
    {
        if (GetComponent<GameManager>().GetNumConnectedPlayers() == 1)
        {
            scoreboard.SetScoreboardMessage("Waiting for players...", Color.white);
            return false;
        }
        else if (numReadyPlayers > 1 && numReadyPlayers >= GetComponent<GameManager>().GetNumConnectedPlayers())
        {
            scoreboard.ClearScoreboardMessage();
            StartGame();
            return true;
        }
        else
        {
            scoreboard.SetNumReadyPlayers(numReadyPlayers, GetComponent<GameManager>().GetNumConnectedPlayers());
            return false;
        }


    }

    private void StartGame()
    {
        scoreboard.StartMatch();
        FindObjectOfType<SoundManager>().StopIdleMusic();
        GetComponent<GameManager>().SetAllPlayersCanMove(true);
        FindObjectOfType<SoundManager>().PlayIntro();
    }

    private void Start()
    {
        FindObjectOfType<SoundManager>().PlayIdleMusic();
        readyPlayerDictionary = new Dictionary<PlayerState, bool>();

        if (earlVictoryPanel != null)
        {
            earlVictoryPanel.SetActive(false);
        }
        if (stellaVictoryPanel != null)
        {
            stellaVictoryPanel.SetActive(false);
        }
    }

    public void StartRound()
    {
        MarkAllPlayersAlive();
    }

    public void MarkAllPlayersAlive()
    {
        numAliveMagenta = numMagentaPlayers;
        numAliveBlack = numBlackPlayers;
    }

    public void MarkPlayerDead(PlayerState player)
    {
        if (player.assignedColor == PlayerColor.Black)
        {
            numAliveBlack--;
            if (numAliveBlack <= 0)
            {
                AwardPointLastStanding(PlayerColor.Magenta);
            }
        }
        else
        {
            numAliveMagenta--;
            if (numAliveMagenta <= 0)
            {
                AwardPointLastStanding(PlayerColor.Black);
            }
        }
    }

    // Award a point to the last standing player
    private void AwardPointLastStanding(PlayerColor winningColor)
    {
        switch (winningColor)
        {
            case PlayerColor.Black:
                blackScore++;
                scoreboard.SetBlackScore(blackScore);
                scoreboard.SetScoreboardMessage("POINT TEAM STELLA", Color.white);
                if (!CheckWinningScore(blackScore))
                {
                    StartCoroutine(ResetPlayingField());
                }
                break;
            case PlayerColor.Magenta:
                magentaScore++;
                scoreboard.SetScoreboardMessage("POINT TEAM EARL", new Color(0.7647f, 0.0902f, 0.7608f, 1.0f));
                scoreboard.SetMagentaScore(magentaScore);
                if (!CheckWinningScore(magentaScore))
                {
                    StartCoroutine(ResetPlayingField());
                }
                break;
            default:
                break;
        }
    }

    bool CheckWinningScore(int score)
    {
        int pointsToWin = GetComponent<GameManager>().pointsToWin;
        if (score >= pointsToWin)
        {
            DeclareWinner(pointsToWin);
            return true;
        }

        return false;
    }

    void DeclareWinner(int pointsToWin = 1)
    {
        if (blackScore > magentaScore)
        {
            scoreboard.SetScoreboardMessage("TEAM STELLA WINS", Color.white);
            if (stellaVictoryPanel != null)
            {
                stellaVictoryPanel.SetActive(true);
            }

        }
        else
        {
            scoreboard.SetScoreboardMessage("TEAM EARL WINS", new Color(0.7647f, 0.0902f, 0.7608f, 1.0f));
            if (earlVictoryPanel != null)
            {
                earlVictoryPanel.SetActive(true);
            }
        }

        StartCoroutine(ResetGame());
    }

    private IEnumerator ResetGame()
    {
        yield return new WaitForSeconds(3.0f);
        
        scoreboard.SetCountdownText(3);
        yield return new WaitForSeconds(1.0f);
        scoreboard.SetCountdownText(2);
        yield return new WaitForSeconds(1.0f);
        scoreboard.SetCountdownText(1);
        yield return new WaitForSeconds(1.0f);
        scoreboard.ClearScoreboardMessage();

        earlVictoryPanel.SetActive(false);
        stellaVictoryPanel.SetActive(false);
        ResetScores();
        
        scoreboard.ClearScoreboardMessage();
        GetComponent<GameManager>().RespawnAllPlayers();

        MarkAllPlayersAlive();
    }

    private IEnumerator ResetPlayingField()
    {
        // TODO: scoreboard show winner
        scoreboard.SetCountdownText(3);
        yield return new WaitForSeconds(1.0f);
        scoreboard.SetCountdownText(2);
        yield return new WaitForSeconds(1.0f);
        scoreboard.SetCountdownText(1);
        yield return new WaitForSeconds(1.0f);
        scoreboard.ClearScoreboardMessage();

        GetComponent<GameManager>().RespawnAllPlayers();

        MarkAllPlayersAlive();
    }

    public PlayerColor GetNeededColor()
    {
        // even teams or less magenta players
        if (numMagentaPlayers <= numBlackPlayers)
        {
            numMagentaPlayers++;
            return PlayerColor.Magenta;
        }
        // less black players
        else
        {
            numBlackPlayers++;
            return PlayerColor.Black;
        }
    }

    public void AddPlayer(PlayerState player)
    {
        CheckAllPlayersReady();
    }

    public void RemovePlayer(PlayerState player)
    {

    }

    public void ResetScores()
    {
        scoreboard.SetBlackScore(0);
        scoreboard.SetMagentaScore(0);
        magentaScore = 0;
        blackScore = 0;
    }


}
