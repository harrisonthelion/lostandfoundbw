﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    public int hP;
    public string teamColor;
    public Weapon equippedWeapon;
    public SpecialAbility equippedSpecial;
    //public AudioClip deathSound;
    private SoundManager soundManSource;

    public float fadeSpeed;
    private Renderer r;
    //public int ammo;

    private BoxCollider2D coll;
    private IEnumerator reloadWeapRoutine;
    private IEnumerator reloadSpRoutine;

    private bool isFaded;

    private void Awake()
    {
        coll = GetComponent<BoxCollider2D>();
        equippedWeapon = GetComponent<Weapon>();
        equippedSpecial = GetComponent<SpecialAbility>();
        r = GetComponent<Renderer>();

        reloadWeapRoutine = equippedWeapon.Reload();
        reloadSpRoutine = equippedSpecial.Reload();

        isFaded = false;

        
    }

    private void OnEnable()
    {
        soundManSource = GameObject.FindGameObjectWithTag("Sound Manager").GetComponent<SoundManager>();
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == teamColor && !isFaded)                     //enter team color and not faded
        {
            //if (reloadWeapRoutine != null)
            //{
                //reloadWeapRoutine = equippedWeapon.Reload();        //begin reload
                StartCoroutine(reloadWeapRoutine);       
            //}
            //if (reloadSpRoutine != null)
            //{
                //begin reload
                //reloadSpRoutine = equippedSpecial.Reload();
                StartCoroutine(reloadSpRoutine);
            //}

            StartCoroutine(FadeOutPlayer());                //begin fade
            isFaded = true;
        }

        else if (collision.tag != teamColor && isFaded)
        {
            if (reloadWeapRoutine != null)
            {
                StopCoroutine(reloadWeapRoutine);                   //stop reloading
            }

            if (reloadSpRoutine != null)
            {
                StopCoroutine(reloadSpRoutine);
            }

            StartCoroutine(FadeInPlayer());                 //begin fade
            isFaded = false;
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        //Debug.Log("exit");
        //if (collision.tag == teamColor)
        //{
            //Debug.Log("left team color");
        //    if (reloadWeapRoutine != null)
        //    {
        //        StopCoroutine(reloadWeapRoutine);                   //stop reloading
        //    }

        //    if (reloadSpRoutine != null)
        //    {
        //        StopCoroutine(reloadSpRoutine);
        //    }

        //    StartCoroutine(FadeInPlayer());                 //begin fade
        //}
    }

    public void Damage(int dmg)
    {
        hP -= dmg;
        if (hP <= 0)
        {
            Die();
        }
    }

    public void SetMovementBounds(Vector2 boundsX, Vector2 boundsY)
    {
        GetComponent<PlayerMovement>().SetBounds(boundsX, boundsY);
    }

    public void Respawn()
    {
        this.gameObject.SetActive(true);
        GetComponent<Weapon>().InstantReload();
        GetComponent<SpecialAbility>().InstantReload();
    }

    public void Die()
    {
        soundManSource.PlayDeathSound();
        this.gameObject.SetActive(false);   //disable controls
        GetComponent<PlayerState>().SetAliveStatus(false);
        //remove avatar from game
    }

    public IEnumerator FadeOutPlayer()
    {
        Debug.Log("Fade Start");
        for (float ft = 1f; ft > 0; ft -= fadeSpeed)
        {
            Color c = r.material.color;
            c.a = ft;
            r.material.color = c;
            yield return null;
        }
        r.enabled = false;
    }

    public IEnumerator FadeInPlayer()
    {
        Debug.Log("Fade Start");
        r.enabled = true;
        for (float ft = 0f; ft < 1; ft += fadeSpeed)
        {
            Color c = r.material.color;
            c.a = ft;
            r.material.color = c;
            yield return null;
        }
    }

}
