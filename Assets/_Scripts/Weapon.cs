﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{

    public int maxAmmo;
    public float reloadTime;
    public float fireRate;

    public GameObject ammoType;
    public GameObject bulletSpawn;
    public AudioClip fireSound;
    public AudioClip emptySound;

    public int currentAmmo;

    private float timeSinceFire;

    private void Awake()
    {
        currentAmmo = maxAmmo;
    }

    void Update()
    {
        timeSinceFire += Time.deltaTime;
    }

    private void Fire()
    {
        //Debug.Log("Fire");
        if (currentAmmo > 0 && timeSinceFire > fireRate)
        {
            GameObject projectile = Instantiate(ammoType, bulletSpawn.transform.position, bulletSpawn.transform.rotation);
            // set self as instigator to prevent self-damage
            projectile.GetComponent<ProjectileBehavior>().instigator = this.gameObject;
            currentAmmo--;
            timeSinceFire = 0f;
            GetComponent<AudioSource>().PlayOneShot(fireSound);
        }
        
        else if (currentAmmo <= 0 && timeSinceFire > fireRate)
        {
            Debug.Log("No Ammo");
            GetComponent<AudioSource>().PlayOneShot(emptySound);
            timeSinceFire = 0f;
        }
    }

    private void OnFire()
    {
        Fire();
    }

    public void InstantReload()
    {
        currentAmmo = maxAmmo;
    }

    public IEnumerator Reload()
    {
        float timeElapsed = 0f;

        while (timeElapsed < reloadTime)
        {
            timeElapsed += Time.deltaTime;

            if (timeElapsed >= reloadTime)
            {
                if (currentAmmo < maxAmmo)
                {
                    currentAmmo++;
                }
                
                timeElapsed = 0f;
            }

            yield return null;
        }

    }
}
