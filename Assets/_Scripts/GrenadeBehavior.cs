﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeBehavior : MonoBehaviour
{
    
    public int damage;
    public float movementSpeed;
    public float range;
    public AudioClip sfx;

    public BoxCollider2D vColl;
    public BoxCollider2D hColl;

    private float distanceTraveled;
    public string color;

    private float delay;
    

    private void Start()
    {
        //rigidbody = GetComponent<Rigidbody2D>();
        distanceTraveled = 0f;
        //color = this.gameObject.GetComponent<Player>().teamColor;
        delay = 0f;
    }


    private void Update()
    {
        //rigidbody.velocity = transform.forward * movementSpeed;
        //rigidbody.AddForce(Vector3.forward * movementSpeed);
        if (distanceTraveled <= range)
        {
            transform.position += transform.up * Time.deltaTime * movementSpeed;
            distanceTraveled += Vector3.Magnitude(transform.up * Time.deltaTime * movementSpeed);
        }


        if (distanceTraveled >= range)
        {
            vColl.enabled = true;
            hColl.enabled = true;

            delay += Time.deltaTime;
            if (delay > .1)
            {
                //Destroy(this.gameObject);   //destroy grenade
                vColl.enabled = false;
                hColl.enabled = false;
                GetComponent<SpriteRenderer>().enabled = false;
            }

            if (delay > 3)
            {
                Destroy(this.gameObject);   //destroy grenade
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collider2d)
    {
        Debug.Log("triggered");
        if (collider2d.tag == "Magenta" || collider2d.tag == "Black" || collider2d.tag == "Neutral")
        {
            Debug.Log("tile hit");
            Explode(collider2d);
            
        }

    }

    public void Explode(Collider2D col)
    {
        Debug.Log("explode with color: " + color);
        col.GetComponent<TileBehavior>().FlipTile(color);
        GetComponent<AudioSource>().PlayOneShot(sfx);
    }

}

    