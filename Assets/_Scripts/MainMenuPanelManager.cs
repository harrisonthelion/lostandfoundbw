﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuPanelManager : MonoBehaviour
{
    public List<GameObject> panelList;

    public GameObject mainMenuPanel;
    // Start is called before the first frame update

    private void TurnOffAllPanels()
    {
        foreach (GameObject gameObject in panelList)
        {
            gameObject.SetActive(false);
        }
    }

    public void GoToMainMenuPanel()
    {
        TurnOffAllPanels();
        mainMenuPanel.SetActive(true);
    }
}
